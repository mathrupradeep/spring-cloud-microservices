package com.springcloud.microservices.currenyexchangeservice.controller;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springcloud.microservices.currenyexchangeservice.beans.ExchangeValue;

@Repository
public interface ExchangeValueRepository extends JpaRepository<ExchangeValue, Integer> {
	ExchangeValue findByFromAndTo(String from, String to);
}
