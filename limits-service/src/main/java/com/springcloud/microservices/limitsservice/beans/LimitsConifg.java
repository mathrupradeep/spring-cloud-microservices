package com.springcloud.microservices.limitsservice.beans;

public class LimitsConifg {

	private int max;
	private int min;

	public LimitsConifg() {
		super();
	}

	public LimitsConifg(int max, int min) {
		super();
		this.max = max;
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

}
