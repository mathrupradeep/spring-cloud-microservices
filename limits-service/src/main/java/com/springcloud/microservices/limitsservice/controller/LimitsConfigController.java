package com.springcloud.microservices.limitsservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springcloud.microservices.limitsservice.beans.LimitsConifg;
import com.springcloud.microservices.limitsservice.config.LimitsConfiguration;

@RestController
@Validated
public class LimitsConfigController {

	@Autowired
	private LimitsConfiguration configuration;

	@GetMapping("/limits")
	public LimitsConifg getLimitsConfig() {
		return new LimitsConifg(configuration.getMax(), configuration.getMin());
	}

}
